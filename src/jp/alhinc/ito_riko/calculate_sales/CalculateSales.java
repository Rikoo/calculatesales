package jp.alhinc.ito_riko.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {
		//コマンドライン引数を使って「ディレクトリの場所」を参照
		//売上集計ファイルを収納している箱が"ディレクトリ"

		//コマンドライン引数が渡されていない場合
		//コマンドライン引数が２個以上の場合.
		//args[0],個数取得できるような状態にする
		if(args.length  != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		//branchsalesmapを作る「
		Map<String,Long> branchsalesmap = new HashMap<String,Long>();
		HashMap<String, String> map = new HashMap<String, String>();


		FileReader fr = null;
		BufferedReader br1 = null;
		//filenamelistを作成(「ファイル名 000001.rcd」をこれから入れるためのリスト)

		if(!fileputin(args[0],"branch.lst",map,branchsalesmap)) {
			return;
		}

		List<File> filenamelist = new ArrayList<>();
		//ディレクトリに含まれるファイルすべてを格納する「filelist」を作成
		File dir = new File(args[0]);
		File[] filelist = dir.listFiles();
		//filelistから連番.rcdを抽出するための処理のはじまり
		for(int i = 0; i < filelist.length; i++) {
			//8桁の連番.rcdを filelistから抽出、ファイル形式のものしか通さないようにする
			if(filelist[i].getName().matches("\\d{8}.rcd") && filelist[i].isFile()){
				//ファイルネームリストにrcdファイルを追加
				filenamelist.add(filelist[i]);
			}
		}
		//ファイルリーダーとバッファードリーダーを初期化

		//filenamelistのrcdファイルの「中身読み込み」して抽出
		//抽出したものをfilerecordlistに追加
		for(int j = 0; j < filenamelist.size() - 1 ; j++){

			//連番チェック(エラー処理)
			//ファイルから文字列の抽出、
			String str1 = new String(filenamelist.get(j).getName());
			String new_str1 = str1.substring(0,8);
			String str2 = new String(filenamelist.get(j+1).getName());
			String new_str2 = str2.substring(0,8);
			int k = Integer.parseInt(new_str1);
			int n = Integer.parseInt(new_str2);
			//連番になっているか、一つ数字足しこんだものとその前の番号を比較
			if( n - k != 1){
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		for(int l = 0; l < filenamelist.size() ; l++){
			try {
				List<String> filerecordlist = new ArrayList<>();
				fr = new FileReader(filenamelist.get(l));
				br1 = new BufferedReader(fr);
				//rcdファイルから売上金額の読み込み
				String line = null;
				while ((line = br1.readLine()) != null){
					//リストに読み込んだものを格納(保持)
					filerecordlist.add(line);
				}

				//rcdファイルの要素数が2個以外だとエラー(行数の問題)
				if(filerecordlist.size() != 2) {
					System.out.println(filenamelist.get(l).getName() + "のフォーマットが不正です");
					return;
				}
				//売上金額が数字以外のもの入っている
				if(!filerecordlist.get(1).matches("[0-9]+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				//支店定義ファイルのコード(エラー処理)
				if(!map.containsKey(filerecordlist.get(0))) {
					System.out.println(filenamelist.get(l).getName() + "の支店コードが不正です");
					return;
				}
				//型変更。ロング型からストリング型へ(mapへ落とし込むため)
				long in = Long.parseLong(filerecordlist.get(1)) + branchsalesmap.get(filerecordlist.get(0));
				if(in > 9999999999L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				branchsalesmap.put(filerecordlist.get(0), in);
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				try{
					if(br1 != null) {
						br1.close();
					}
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		if(!fileoutput(args[0],"branch.out",map,branchsalesmap)) {
			return;
		}
	}
	public static boolean fileoutput(String path,String filename,HashMap<String, String> map,Map<String,Long>branchsalesmap){

		BufferedWriter bw = null;
		FileWriter filewriter = null;

		try{//branchoutのファイルを作る。入力作業のはじまり
			File salefile = new File(path,filename);
			//salefile
			filewriter = new FileWriter(salefile);
			bw = new BufferedWriter(filewriter);
			for(String code : map.keySet()) {
					bw.write(code + "," + map.get(code) +  ","  + branchsalesmap.get(code));
					bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			try{
				if(bw != null) {
				bw.close();
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}
		return true;
	}

	public static boolean fileputin (String path,String file,HashMap<String, String>map,Map<String,Long>branchsalesmap) {
		BufferedReader br = null;

		try{
			File bfile = new File(path,file);
		    //支店定義ファイルの読み込みエラー処理
			if(!bfile.exists()){
			System.out.println("支店定義ファイルが存在しません");
			return false;
		}
			//FileReaderを使ってbFile(branchlst支店定義ファイル)の中身を読み込む
			FileReader fr = new FileReader(bfile);
			//BufferedReaderを使ってテキストの読み込み
			br = new BufferedReader(fr);
			String line;
			//支店定義ファイルの支店コードと支店名横並びを解除、カンマで区切る方法に
			while ((line = br.readLine()) != null) {
				String[] branch = line.split(",");
				//支店定義ファイルの中、フォーマット不正の場合におけるエラー処理
				//数値以外の値
				//アルファベット、ひらがな、カタカナ、漢字、記号が含まれる場合
				//桁数が二桁以下
				//桁数が四桁以上
				//要素数が一つ以下 配列の要素数取得するメゾットを利用args[0]
				//要素数が三つ以上
				if((!branch[0].matches("[0-9]{3}")) || (branch.length != 2)){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				map.put(branch[0], branch[1]);
				//branchsalesmapに支店コードと元々の合計金0を用意、売上金と足しこんでいく準備
				branchsalesmap.put(branch[0],(long) 0);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			try{
				if(br != null) {
					br.close();
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}
		return true;
	}
}



